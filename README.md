# GP-Framework-Glow

Geometry Processing practical assignments using GLOW.

# Build Instructions
```
git clone https://www.graphics.rwth-aachen.de:9000/Teaching/gp-framework-glow.git
cd gp-framework-glow
git submodule update --init --recursive
mkdir build
cd build
cmake ..
make
```