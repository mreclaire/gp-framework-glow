#include "task.hh"

#include <iostream>
#include <queue>

#include <typed-geometry/feature/matrix.hh>
#include <typed-geometry/feature/std-interop.hh>
#include <typed-geometry/feature/vector.hh>

bool task::is_delaunay(polymesh::edge_handle edge, pm::vertex_attribute<tg::pos2> const& position)
{
    auto const va = edge.halfedgeA().next().vertex_to();
    auto const vc = edge.halfedgeA().vertex_to();
    auto const vd = edge.halfedgeB().next().vertex_to();
    auto const vb = edge.halfedgeB().vertex_to();

    /* These are the four points of the triangles incident to edge _eh
            a
           / \
          /   \
        b ----- c
          \   /
           \ /
            d
    */
    tg::pos2 const& a = position[va];
    tg::pos2 const& b = position[vb];
    tg::pos2 const& c = position[vc];
    tg::pos2 const& d = position[vd];

    bool result = true;

    // IMPORTANT: DO NOT ADD ANY CODE OUTSIDE OF THE MARKED CODE STRIPS
    // INSERT CODE:
    // is the edge delaunay or not?
    // -> circum-circle test of the four points (a,b,c,d) OR check if the projected paraboloid is convex
    //--- start strip ---
    /*// calculation using angles
    // calculate angles
    double alpha, delta;
    alpha = tg::angle_between(b-a, c-a).radians();
    delta = tg::angle_between(c-d, b-d).radians();

    // if sum is >180°, edge needs to be flipped
    // -> alpha + delta < beta + gamma means edge is delaunay
    result = ((alpha+delta) <= tg::pi<double>.radians());*/

    // calculation using projection to paraboloid
    using pos3 = tg::pos3;
    using vec3 = tg::vec3;

    // lift a,b,c,d onto paraboloid
    pos3 a_para = pos3(a.x, a.y, a.x * a.x + a.y * a.y);
    pos3 b_para = pos3(b.x, b.y, b.x * b.x + b.y * b.y);
    pos3 c_para = pos3(c.x, c.y, c.x * c.x + c.y * c.y);
    pos3 d_para = pos3(d.x, d.y, d.x * d.x + d.y * d.y);

    // normal vector of lifted tri
    vec3 n_abc = tg::cross(c_para - b_para, a_para - b_para);

    // vector a--d
    vec3 a_d = d_para - a_para;

    // check if b--c is convex with dot of n_abc and a_d
    auto dot = tg::dot(n_abc, a_d);

    if (dot <= 0) {
        // edge b--c is concave => edge is not delaunay
        result = false;
    }

    //--- end strip ---

    return result;
}

polymesh::vertex_index task::insert_vertex(polymesh::Mesh& mesh, pm::vertex_attribute<tg::pos2>& position, tg::pos2 const& vertex_position, polymesh::face_handle face)
{
    // add vertex and assign it its position
    auto const v = mesh.vertices().add();
    position[v] = vertex_position;

    if (face.is_valid())
    {
        mesh.faces().split(face, v);
        std::cout << "[delaunay] 1:3 Split: vertex " << v.idx.value << " at position " << vertex_position << " inside triangle " << face.idx.value << std::endl;
    }
    else
        return {};

    // IMPORTANT: DO NOT ADD ANY CODE OUTSIDE OF THE MARKED CODE STRIPS
    // INSERT CODE:
    // re-establish Delaunay property
    // ... find edges opposite to the inserted vertex
    // ... are these edges ok? otherwise: flip'em (use mesh.edges().flip(the_edge_to_flip))
    // ... propagate if necessary
    // Hint:
    //   Use is_delaunay(...) (see above) to check the delaunay criterion.
    //   Do not check boundary edges as they do not neighbor two triangles.
    //   You can check if an edge e is boundary by calling e.is_boundary()
    //   You can use an std::queue as a container for edges
    //--- start strip ---
    // some definitions
    namespace pm = polymesh;
    using EH = pm::edge_handle;
    using HEH = pm::halfedge_handle;

    // define queue
    std::queue<EH> q;
    pm::edge_attribute<bool> inQueue(mesh, false); // to avoid putting one edge multiple times into the queue

    // iterate over all outgoing halfedges to add necessary edges to the queue
    // vertex is always inserted on face
    for (HEH he : v.outgoing_halfedges())
    {
        EH edge = he.next().edge();
        if (edge.is_boundary()) continue;
        q.push(edge);
        inQueue[edge] = true;
    }

    // do while queue is not empty
    while (!q.empty())
    {
        // get first element from queue
        EH cur_edge = q.front();
        q.pop();
        inQueue[cur_edge] = false;

        // if edge is delaunay, continue
        if (is_delaunay(cur_edge, position)) continue;

        // if edge is not delaunay, flip
        mesh.edges().flip(cur_edge);

        // add neighbouring edges to queue
        EH surrounding_edges[4] = {cur_edge.halfedgeA().next().edge(), cur_edge.halfedgeA().next().next().edge(),
                                   cur_edge.halfedgeB().next().edge(), cur_edge.halfedgeB().next().next().edge()};
        for (EH se : surrounding_edges)
        {
            if ((!inQueue[se]) && !se.is_boundary())
            {
                q.push(se);
                inQueue[se] = true;
            }
        }
    }
    //--- end strip ---

    return v;
}
