# Enumerate all team members in this file, one per line, using
# the format <matriculation number> <name>.
# Empty lines and lines starting with a hash symbol '#' are ignored.
# Below is an example entry. Delete it before adding your own entries!

406611 Hannah Meinhold
357697 Moritz Reclaire
343818 Johannes Schulten
342423 Peter Weichenthal

