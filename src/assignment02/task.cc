#include "task.hh"

#include <typed-geometry/tg.hh>

#include <iostream>

tg::dir3 task::compute_normal(std::vector<pm::vertex_handle> const& vs, pm::vertex_attribute<tg::pos3> const& position)
{
    // the normal to be computed
    tg::dir3 normal;

    /*
     * INSERT YOUR OWN CODE BETWEEN THE HORIZONTAL LINES BELOW.
     * DO NOT CHANGE CODE ANYWHERE ELSE.
     *
     * Note that there is another function below this function
     * where you have to insert code as well.
     *
     * This function should compute a regression plane for the
     * supplied sequence of points and return an arbitrary one
     * of the two normal vectors of that plane. (The orientation
     * doesn't matter at this point.)
     *
     * Hints:
     *   use tg::mat3 as 3x3 (column major) matrix representation
     *   tg::eigen_decomposition_symmetric may save you a lot of time
     *
     */
    // ----- %< -------------------------------------------------------

    // ompute the centroid of the points
    tg::pos3 centroid;;
    for (auto v : vs) {
        centroid += position[v];
    }
    centroid /= vs.size();

    // compute the covariance matrix
    tg::mat3 covariance;
    for (auto v : vs) {
        tg::vec3 deviation = position[v] - centroid;
        covariance[0][0] += deviation.x * deviation.x;
        covariance[0][1] += deviation.x * deviation.y;
        covariance[0][2] += deviation.x * deviation.z;
        covariance[1][0] += deviation.y * deviation.x;
        covariance[1][1] += deviation.y * deviation.y;
        covariance[1][2] += deviation.y * deviation.z;
        covariance[2][0] += deviation.z * deviation.x;
        covariance[2][1] += deviation.z * deviation.y;
        covariance[2][2] += deviation.z * deviation.z;
    }

    // compute eigenvectors and eigenvalues
    auto results = tg::eigen_decomposition_symmetric(covariance);

    // sort eigenvectors by eigenvalues
    std::sort(results.begin(), results.end(), [](auto& lhs, auto& rhs) {
        return lhs.eigenvalue < rhs.eigenvalue;
    });
        CC_ASSERT(results[0].eigenvalue <= results[1].eigenvalue);
    CC_ASSERT(results[0].eigenvalue <= results[2].eigenvalue);

    // choose the eigenvector with the largest eigenvalue for the normal
    normal = tg::normalize(results[0].eigenvector);

    // ----- %< -------------------------------------------------------
    /*
     *
     * Insert your own code above.
     * NO CHANGES BEYOND THIS POINT!
     *
     */

    return normal;
}

float task::compute_mst_weight(pm::vertex_handle v0, pm::vertex_handle v1, pm::vertex_attribute<tg::pos3> const& position, pm::vertex_attribute<tg::dir3> const& normal)
{
    // this is the weight that you should overwrite
    float weight;

    /*
     * INSERT YOUR OWN CODE BETWEEN THE HORIZONTAL LINES BELOW.
     * DO NOT CHANGE CODE ANYWHERE ELSE.
     *
     * This function should compute the weight of the edge between
     * the two supplied vertices for the purpose of the normal
     * orientation propagation algorithm using the minimum spanning tree.
     *
     */
    // ----- %< -------------------------------------------------------

    auto distance = tg::length(position[v1] - position[v0]);
    auto dot = tg::dot(normal[v1], normal[v0]);
    weight = distance + (1 - abs(dot));

    // ----- %< -------------------------------------------------------
    /*
     *
     * Insert your own code above.
     * NO CHANGES BEYOND THIS POINT!
     *
     */

    return weight;
}
