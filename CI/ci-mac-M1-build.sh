#!/bin/bash

PATH=/opt/homebrew/bin:/opt/homebrew/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin

export PATH

export LD_LIBRARY_PATH=/opt/homebrew/lib:/opt/local/lib

# Script abort on error
set -e


echo -e "${OUTPUT}"
echo ""
echo "======================================================================"
echo "Basic configuration details:"
echo "======================================================================"
echo -e "${NC}"

echo "This is an Mac M1 build"

cmake --version

echo "Current PATH variable:"
echo "$PATH"
echo ""

echo "Current LD_LIBRARY_PATH variable:"
echo "$LD_LIBRARY_PATH"
echo ""

echo -e "${OUTPUT}"
echo ""
echo "======================================================================"
echo "Building Release version"
echo "======================================================================"
echo -e "${NC}"

echo -e "${OUTPUT}"
echo ""
echo "======================================================================"
echo "Configuring"
echo "======================================================================"
echo -e "${NC}"


if [ ! -d build-release ]; then
  mkdir build-release
fi

cd build-release

cmake ..

cmake --build .
